package ldh.maker.handle;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import ldh.maker.util.UiUtil;

import java.io.IOException;

/**
 * Created by ldh on 2017/2/25.
 */
public class DbHandle {

    public static void newConnection(MenuItem createConnection) {
        createConnection.setOnAction(e->{
            Parent root = null;
            try {
                Stage stage = new Stage();
                stage.initOwner(UiUtil.STAGE);
                root = FXMLLoader.load(DbHandle.class.getResource("/fxml/CreateNewConnectionForm.fxml"));
                Scene scene = new Scene(root, 480, 200);
                stage.setFullScreen(false);
                stage.setResizable(false);
                stage.setTitle("创建数据库连接");
                stage.setScene(scene);
                stage.show();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
    }
}
