<template>
  <div>
    <table-view :headColumns="headColumns" :rowDatas="rowDatas" v-bind:total="total"
                v-bind:pageSize="pageSize" v-bind:currentPageNum="currentPageNum" v-bind:pageNumShow="pageNumShow"
                v-bind:pageSizeList="pageSizeList" v-on:loadData="loadData"></table-view>
  </div>
</template>

<script>
  import TableView from  '@/components/TableView'

  export default {
    name: '${util.firstLower(table.javaName)}-list',
    components: {
      "table-view": TableView
    },
    data () {
      return {
        headColumns: [
        <#list table.columnList as column>
          <#if column.create>
            <#if column.foreign>
            {'label': '${column.property}.${column.foreignKey.foreignTable.columnList[0].property}', 'field': '${util.comment(column)}'<#if column.width??>, 'width':${column.width}</#if>},
            <#else>
            {'label': '${column.property}', 'field': '${util.comment(column)}'<#if column.width??>, 'width': ${column.width}</#if>},
            </#if>
          </#if>
        </#list>
          {'label': '操作', 'field': 'opt', 'width': 200}
          ],
        rowDatas: [
        ],
        pageSize: 10, currentPageNum: 1, pageNumShow: 10, pageSizeList: [10, 20, 30, 50, 100]
      }
    },
    mounted(){ //mounted
       this.initData(1, 10);
    },
    methods: {
      loadData:function(pageNum, pageSize){
        this.initData(pageNum, pageSize);
      },
      initData:function(pageNum, pageSize) {
        var that=this;
        $.ajax({
          type:"get",
          data: {"pageNo": pageNum, "pageSize": pageSize},
          datatype:"JSON",
          crossDomain: true,
          url:"http://localhost:8282/${util.firstLower(table.javaName)}/listJson",
          success:function(data){
            that.rowDatas=that.addOper(data.beans);
            that.total = data.total;
            that.currentPageNum = data.pageNo
            that.pageSize = data.pageSize;
          }
        })
      },
      addOper:function(data) {
        var that=this;
          for(var dd in data) {
            console.info(data[dd])
              data[dd].opt = "<a href=/#/${util.firstLower(table.javaName)}/view?id=" + data[dd].id  + ">编辑</a>";
          }
          return data;
      }
    }
  }
</script>
